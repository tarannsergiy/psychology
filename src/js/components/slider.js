import Swiper, {Navigation, Pagination} from 'swiper';

class Slider {
    static defaultProps = {
        modules: [Pagination],
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    }

    constructor(selector = '',  options = {} ) {
        this.selector = selector;
        this.options = {...Slider.defaultProps, ...options};

        this.initSlider();
    }

    initSlider () {
        if (!this.selector) {
            return;
        }

        new Swiper(
            this.selector,
            this.options
        );
    }
}

export {Slider, Navigation, Pagination};
