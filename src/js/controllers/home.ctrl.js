import {Slider} from "../components/slider.js";
import Accordion from "../components/accordion.js";

class HomeCtrl {
    constructor() {
        this.init();
    }

    init () {
        this.initReviewsSlider();
        this.initPriceSlider();
        this.initPersonsSlider();
        this.initServiceAccordion();
    }

    initReviewsSlider () {
        const options = {
            effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: "auto",
            coverflowEffect: {
                rotate: 0,
                stretch: -60,
                depth: 200,
                modifier: 1,
                slideShadows: false
            },
        };

        new Slider('.js-reviews-slider', options);
    }

    initPriceSlider () {
        const options = {
            slidesPerView: 3,
            spaceBetween: 0,
            autoHeight: true,
        };

        new Slider('.js-price-slider', options);
    }

    initPersonsSlider () {
        const options = {
            slidesPerView: 3,
            spaceBetween: 0,
            autoHeight: true,
        };

        new Slider('.js-persons-slider', options);
    }

    initServiceAccordion () {
        const accordionDomNode = document.querySelector('.js-badger-accordion'),
        options = {
            openHeadersOnLoad: [0],
        }

        new Accordion(accordionDomNode, options);
    }
}

export default HomeCtrl;