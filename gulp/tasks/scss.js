import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import rename from 'gulp-rename';
import gcmq from 'gulp-group-css-media-queries';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';

const sass = gulpSass(dartSass);

export const scss = () => {
    return app.gulp.src(app.path.src.scss)
        .pipe(
            app.plugins.if(
                app.isDev,
                sourcemaps.init()
            )
        )
        .pipe(
            app.plugins.plumber(
                app.plugins.notify.onError(
                    {
                        title: 'SCSS',
                        message: 'Error: <%= error.message %>',
                    }
                )
            )
        )
        .pipe(
            sass(
                {
                    outputStyle: 'expanded',
                }
            )
        )
        .pipe(
            app.plugins.if(
                app.isBuild,
                gcmq()
            )
        )
        .pipe(
            autoprefixer(
                {
                    grid: true,
                    cascade: true,
                }
            )
        )
        .pipe(
            app.plugins.if(
                app.isBuild,
                cleanCSS()
            )
        )
        .pipe(
            rename(
                {
                    extname: '.min.css',
                }
            )
        )
        .pipe(
            app.plugins.if(
                app.isDev,
                sourcemaps.write()
            )
        )
        .pipe(app.gulp.dest(app.path.build.css))
        .pipe(app.plugins.browsersync.stream());
};
