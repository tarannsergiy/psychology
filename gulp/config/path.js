import * as path from 'path';
const rootFolder = path.basename(path.resolve());
console.log(rootFolder);
const buildFolder = './dist';
const srcFolder = './src';

export const pathConfig = {
    build: {
        icons: `${buildFolder}/icons/`,
        img: `${buildFolder}/img/`,
        js: `${buildFolder}/js/`,
        css: `${buildFolder}/css/`,
        html: `${buildFolder}/`,
        files: `${buildFolder}/files/`,
        fonts: `${buildFolder}/fonts/`,
    },
    src: {
        pngIcons2x: `${srcFolder}/png-icons/**/*@2x.png`,
        pngIcons: `${srcFolder}/png-icons/**/*.png`,
        svgMulti: `${srcFolder}/svg-icons/multi/*.svg`,
        svgMono: `${srcFolder}/svg-icons/mono/*.svg`,
        img: `${srcFolder}/img/**/*.{jpg,jpeg,png,gif,svg}`,
        js: `${srcFolder}/js/index.js`,
        scss: `${srcFolder}/scss/[^_]*.scss`,
        html: `${srcFolder}/njk/[^_]*.njk`,
        files: `${srcFolder}/files/**/*.*`,
        fonts: `${srcFolder}/fonts/**/*.*`,
    },
    watch: {
        pngIcons: `${srcFolder}/png-icons/**/*.png`,
        svgMulti: `${srcFolder}/svg-icons/multi/*.svg`,
        svgMono: `${srcFolder}/svg-icons/mono/*.svg`,
        img: `${srcFolder}/img/**/*.{jpg,jpeg,png,gif,svg}`,
        js: `${srcFolder}/js/**/*.js`,
        scss: `${srcFolder}/scss/**/*.scss`,
        html: `${srcFolder}/njk/**/*.njk`,
        files: `${srcFolder}/files/**/*.*`,
        fonts: `${srcFolder}/files/**/*.*`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    njkFolder: `${srcFolder}/njk`,
    scssMixinsFolder: `${srcFolder}/scss/mixins`,
}